﻿using com.fonda.caiwu.common;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace caiwu
{
    public partial class Form1 : DevExpress.XtraEditors.XtraForm
    {
        public string str;
        public Form1()
        {
            //str = "woshijingjing";
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                string excelFile = fd.FileName;

                string sheetName = "chuku";
                string shopxq = sheetName + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                ConfigHelper.UpdateAppConfig("shopxq", shopxq);
                DataSet ds = new DataSet();
                try
                {
                    //获取全部数据     
                    //  string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + excelFile + ";" + "Extended Properties='Excel 8.0;IMEX=1'";
                    string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFile + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1';"; // Office 07及以上版本 不能出现多余的空格 而且分号注意
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    string strExcel = "";
                    OleDbDataAdapter myCommand = null;
                    strExcel = string.Format("select * from [{0}$]", sheetName);
                    myCommand = new OleDbDataAdapter(strExcel, strConn);
                    myCommand.Fill(ds, sheetName);
                    //  this.sheet1name = sheetName + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString(); ;
                    // ConfigHelper.UpdateAppConfig("sheet0", this.sheet1name);
                    //如果目标表不存在则创建,excel文件的第一行为列标题,从第二行开始全部都是数据记录     
                    string strSql = string.Format("create table {0}(", shopxq);   //以sheetName为表名     

                    string strInsql = string.Format("insert into  {0} ", shopxq);
                    foreach (System.Data.DataColumn c in ds.Tables[0].Columns)
                    {
                        strSql += string.Format("{0} TEXT,", c.ColumnName);
                        //    strInsql += string.Format("{0}  ,", c.ColumnName);
                    }
                    strSql = strSql.Trim(',') + ")";
                    //   MySqlHelper.ExecuteNonQuery(MySqlHelper.Conn, CommandType.Text, strSql, null);
                    DbHelperSQLite.ExecuteSql(strSql);
                    using (SQLiteConnection conns = new SQLiteConnection(DbHelperSQLite.connectionString))
                    {
                        conns.Open();
                        using (SQLiteTransaction trans = conns.BeginTransaction())
                        {
                            try
                            {
                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    DataRow dr;
                                    
                              
                                    dr = ds.Tables[0].Rows[i];
                                    insertToSql(dr, strInsql, conns, trans);

                                }
                                trans.Commit();
                            }
                            catch (System.Data.SQLite.SQLiteException E)
                            {

                                trans.Rollback();
                                throw new Exception(E.Message);
                            }
                        }
                    }
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    //  dt.Columns.Add("jingkai");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow row = dt.Rows[i];
                    }
                    // dataGridView1.DataSource = dt;
                    gridControl1.DataSource = dt;
                    MessageBox.Show("成功导入");
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
        }
        public static void insertToSql(DataRow dr, string insql, SQLiteConnection conn, SQLiteTransaction trans)
        {
            //excel表中的列名和数据库中的列名一定要对应  
            object[] arr = dr.ItemArray;
            string sqlin = null;
            for (int i = 0; i < arr.Length; i++)
            {
                sqlin += "\"" + arr[i] + "\"";
                if (i != arr.Length - 1) { sqlin += ","; }

            }
            // Console.WriteLine(sqlin);
            // MessageBox.Show(sqlin);
            string sql = insql + "values (" + sqlin + ")";
            //   Console.WriteLine(sql);
            DbHelperSQLite.Query(conn, sql, trans);
            //  Console.WriteLine(sql);
            //SqlCommand cmd = new SqlCommand(sql, conn);
            //cmd.ExecuteNonQuery();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            str = "6666";
            XtraForm1 x= new XtraForm1();
            x.str = str;
            x.Show();
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            str = "woshijingkai";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XtraForm2 xx = new XtraForm2();
            xx.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            XtraForm3 xxs = new XtraForm3();
            xxs.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PrintableComponentLink link = new PrintableComponentLink(new PrintingSystem());
            link.Component = gridControl1;
            link.Landscape = true;
            PageHeaderFooter phf = link.PageHeaderFooter as PageHeaderFooter;
            phf.Header.Content.Clear();
            phf.Header.Content.AddRange(new string[] { "",  "站点信息表", "" });
            phf.Header.Font = new System.Drawing.Font("宋体", 16, System.Drawing.FontStyle.Regular);
            phf.Header.LineAlignment = BrickAlignment.Center;
            phf.Footer.Content.Clear();
            phf.Footer.Content.AddRange(new string[] { "", String.Format("打印时间: {0:g}", DateTime.Now), "" });
            link.CreateDocument();
            link.ShowPreview();

        }
    }
}
