﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using com.fonda.caiwu.common;
using System.Threading;
using com.gfonda.caiwu.utility;
using System.Net;
using System.IO;
using System.Drawing.Imaging;
using System.Reflection;
using System.Collections;
using System.Data.Entity.Core.Objects;
using DevExpress.XtraReports.UI;
using com.gfonda.caiwu.common.dto;

namespace WindowsFormsCaiWuApplication
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        //定义一个为委托
        public delegate void Entrust(string str);
        private delegate void SetTextCallback(string text);
        shopEntities sp = new shopEntities();
        public MainForm()
        {    
            //此时禁用了所有的控件合法性检查。
            //System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        
        /// <summary>
        /// 当前月份的数据导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
             //打开文件导入
              shopEntities sp = new shopEntities();
            // Entrust callback = new Entrust(CallBack); //把方法赋值给委托
          //  Entrust callback = new Entrust(ImportCallBack); //把方法赋值给委托
            // Thread thread1 = new Thread(Thread1);
                                                      //调用Start方法执行线程
                                                      //thread1.Start(callback);

            //  var so = new shop_order {
            //      id = 1,dingdanbianhao="123"

            //  };
            //  //  shop_order so = new shop_order();
            //  //  so.dingdanbianhao = "1233";
            //  sp.shop_order.Add(so);
            //  sp.SaveChanges();
            //  // sp.
            //  // string sql = "select * from shop_order";
            ////  var data = sp.Database.ExecuteSqlCommand(sql);
            //    var sss=  sp.shop_orders.Where(s=>s.id>5).ToList();
            //  gridControl1.DataSource = sss;
            //考虑使用多线程
            //   DataTable dt=new DataTable();
            OpenFileDialog fd = new OpenFileDialog();
              if (fd.ShowDialog() == DialogResult.OK)
              {
                  string excelFile = fd.FileName;

                //try {

                //    dt =   NpoiHelper.Import(excelFile);
                //  }
                //  catch(Exception ex) {
                //      MessageBox.Show(ex+"文档有误！！");
                //  }
                //  progressBarControl2.Visible = true;
                //  //设置一个最小值
                //  progressBarControl2.Properties.Minimum = 0;
                //  //设置一个最大值
                //  progressBarControl2.Properties.Maximum = dt.Rows.Count;
                //  //设置步长，即每次增加的数
                //  progressBarControl2.Properties.Step = 1;
                //  progressBarControl2.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
                //  //当前值
                //  progressBarControl2.Position = 0;
                //  //是否显示进度数据
                //  progressBarControl2.Properties.ShowTitle = true;
                //  //是否显示百分比
                //  progressBarControl2.Properties.PercentView = true;
                //  //初次导入
                //  foreach (DataRow dr in dt.Rows) {
                //    //   string abc = dr["订单编号"].ToString();
                //    // DataTable.ImportRow(dr);//复制行数据到新表     
                //    //var so = new orders
                //    //{
                //    //    //提示 = dr["提示"].ToString(),
                //    //    //下单员 = dr["下单员"].ToString(),
                //    //    //业务员 = dr["业务员"].ToString(),
                //    //    //买家ID = dr["买家ID"].ToString(),
                //    //    //买家地址 = dr["买家地址"].ToString(),
                //    //    //买家留言 = dr["买家留言"].ToString(),
                //    //    //产品总金额 = double.Parse(dr["产品总金额"].ToString()),
                //    //    //仓库 = dr["仓库"].ToString(),
                //    //    //仓库名称 = dr["仓库名称"].ToString(),
                //    //    //付款日期 = dr["付款日期"].ToString(),
                //    //    //付款状态 = dr["付款状态"].ToString(),
                //    //    //代金券 = dr["代金券"].ToString(),
                //    //    //优惠券 = dr["优惠券"].ToString(),
                //    //    //体积 = dr["体积"].ToString(),
                //    //    //佣金 = dr["佣金"].ToString(),
                //    //    //促销渠道 = dr["促销渠道"].ToString(),
                //    //    //其他备注 = dr["其他备注"].ToString(),
                //    //    //内部便签 = dr["内部便签"].ToString(),
                //    //    //到款员 = dr["到款员"].ToString(),
                //    //    //区 = dr["区"].ToString(),
                //    //    //区县区域代码 = dr["区县区域代码"].ToString(),
                //    //    //单品数量 = dr["单品数量"].ToString(),
                //    //    //单品条数 = dr["单品条数"].ToString(),
                //    //    //发货日期 = dr["发货日期"].ToString(),
                //    //    //发货状态 = dr["发货状态"].ToString(),
                //    //    //取消员 = dr["取消员"].ToString(),
                //    //    //取消时间 = dr["取消时间"].ToString(),
                //    //    //合并状态 = dr["合并状态"].ToString(),
                //    //    //商家承担优惠合计 = dr["商家承担优惠合计"].ToString(),
                //    //    //处理状态 = dr["处理状态"].ToString(),
                //    //    //外部平台单号 = dr["外部平台单号"].ToString(),
                //    //    //外部订单状态 = dr["外部订单状态"].ToString(),
                //    //    //实付快递费 = dr["实付快递费"].ToString(),
                //    //    //实收快递费 = dr["实收快递费"].ToString(),
                //    //    //实收金额 = double.Parse(dr["实收金额"].ToString()),
                //    //    //审单员 = dr["审单员"].ToString(),
                //    //    //审单时间 = dr["审单时间"].ToString(),
                //    //    //客服备注 = dr["客服备注"].ToString(),
                //    //    //已打印 = dr["已打印"].ToString(),
                //    //    //市 = dr["市"].ToString(),
                //    //    //市区域代码 = dr["市区域代码"].ToString(),
                //    //    //平台发货状态 = dr["平台发货状态"].ToString(),
                //    //    //平台承担优惠合计 = dr["平台承担优惠合计"].ToString(),
                //    //    //应付快递费 = dr["应付快递费"].ToString(),
                //    //    //应付记账日期 = dr["应付记账日期"].ToString(),
                //    //    //应收快递费 = dr["应收快递费"].ToString(),
                //    //    //应收核销日期 = dr["应收核销日期"].ToString(),

                //    // dt.Clone;





                //    //};
                //    //  System.Threading.Thread.Sleep(12);
                //    orders s = new orders();
                //    s = D2E<orders>(dr);
                //    s.add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //    var so =s ;
                //    sp.orders.Add(so);
                //    // sp.shop_order.Add(so);
                //    sp.SaveChanges();
                //   // progressBarControl2.Position += 1;
                //    progressBarControl2.PerformStep();
                //    //处理当前消息队列中的所有windows消息,不然进度条会不同步
                //    System.Windows.Forms.Application.DoEvents();
                //}
                Thread importExcel = new Thread(new ParameterizedThreadStart(ImportExcel)); ;
                importExcel.Start(excelFile);
        }
//            textEdit1.Text = "666";
        }


        public static T D2E<T>(DataRow r)
        {
            T t = default(T);
            t = Activator.CreateInstance<T>();
            PropertyInfo[] ps = t.GetType().GetProperties();
            foreach (var item in ps)
            {
                if (r.Table.Columns.Contains(item.Name))
                {
                    object v = r[item.Name];
                    if (v.GetType() == typeof(System.DBNull))
                        v = null;

                   Console.WriteLine(item.PropertyType.Name);
                    if (item.PropertyType.Name.StartsWith("Nullable`1")) {
                        item.SetValue(t, double.Parse(v.ToString()), null);

                    } else { 
                    item.SetValue(t, v, null);
                    }
                }
            }
            return t;
        }

        /// <summary>
        /// 多线程导入数据，通过委托
        /// </summary>
        /// <param name="obj"></param>
        public void ImportExcel(object excelFile) {
            DataTable dt = new DataTable();
          //  Entrust callback = obj as Entrust;//强转为委托
            try
            {

                dt = NpoiHelper.Import(excelFile.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "文档有误！！");
            }
            this.Invoke((EventHandler)delegate
            {
                progressBarControl2.Visible = true;
                //设置一个最小值
                progressBarControl2.Properties.Minimum = 0;
                //设置一个最大值
                progressBarControl2.Properties.Maximum = dt.Rows.Count;
                //设置步长，即每次增加的数
                progressBarControl2.Properties.Step = 1;
                progressBarControl2.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
                //当前值
                progressBarControl2.Position = 0;
                //是否显示进度数据
                progressBarControl2.Properties.ShowTitle = true;
                //是否显示百分比
                progressBarControl2.Properties.PercentView = true;
                //初次导入
                foreach (DataRow dr in dt.Rows)
                {
                    //   string abc = dr["订单编号"].ToString();
                    // DataTable.ImportRow(dr);//复制行数据到新表     
                    //var so = new orders
                    //{
                    //    //提示 = dr["提示"].ToString(),
                    //    //下单员 = dr["下单员"].ToString(),
                    //    //业务员 = dr["业务员"].ToString(),
                    //    //买家ID = dr["买家ID"].ToString(),
                    //    //买家地址 = dr["买家地址"].ToString(),
                    //    //买家留言 = dr["买家留言"].ToString(),
                    //    //产品总金额 = double.Parse(dr["产品总金额"].ToString()),
                    //    //仓库 = dr["仓库"].ToString(),
                    //    //仓库名称 = dr["仓库名称"].ToString(),
                    //    //付款日期 = dr["付款日期"].ToString(),
                    //    //付款状态 = dr["付款状态"].ToString(),
                    //    //代金券 = dr["代金券"].ToString(),
                    //    //优惠券 = dr["优惠券"].ToString(),
                    //    //体积 = dr["体积"].ToString(),
                    //    //佣金 = dr["佣金"].ToString(),
                    //    //促销渠道 = dr["促销渠道"].ToString(),
                    //    //其他备注 = dr["其他备注"].ToString(),
                    //    //内部便签 = dr["内部便签"].ToString(),
                    //    //到款员 = dr["到款员"].ToString(),
                    //    //区 = dr["区"].ToString(),
                    //    //区县区域代码 = dr["区县区域代码"].ToString(),
                    //    //单品数量 = dr["单品数量"].ToString(),
                    //    //单品条数 = dr["单品条数"].ToString(),
                    //    //发货日期 = dr["发货日期"].ToString(),
                    //    //发货状态 = dr["发货状态"].ToString(),
                    //    //取消员 = dr["取消员"].ToString(),
                    //    //取消时间 = dr["取消时间"].ToString(),
                    //    //合并状态 = dr["合并状态"].ToString(),
                    //    //商家承担优惠合计 = dr["商家承担优惠合计"].ToString(),
                    //    //处理状态 = dr["处理状态"].ToString(),
                    //    //外部平台单号 = dr["外部平台单号"].ToString(),
                    //    //外部订单状态 = dr["外部订单状态"].ToString(),
                    //    //实付快递费 = dr["实付快递费"].ToString(),
                    //    //实收快递费 = dr["实收快递费"].ToString(),
                    //    //实收金额 = double.Parse(dr["实收金额"].ToString()),
                    //    //审单员 = dr["审单员"].ToString(),
                    //    //审单时间 = dr["审单时间"].ToString(),
                    //    //客服备注 = dr["客服备注"].ToString(),
                    //    //已打印 = dr["已打印"].ToString(),
                    //    //市 = dr["市"].ToString(),
                    //    //市区域代码 = dr["市区域代码"].ToString(),
                    //    //平台发货状态 = dr["平台发货状态"].ToString(),
                    //    //平台承担优惠合计 = dr["平台承担优惠合计"].ToString(),
                    //    //应付快递费 = dr["应付快递费"].ToString(),
                    //    //应付记账日期 = dr["应付记账日期"].ToString(),
                    //    //应收快递费 = dr["应收快递费"].ToString(),
                    //    //应收核销日期 = dr["应收核销日期"].ToString(),

                    // dt.Clone;





                    //};
                    //  System.Threading.Thread.Sleep(12);
                    using (var contextTransaction=sp.Database.BeginTransaction()) {
                        try
                        {
                            orders s = new orders();
                            s = D2E<orders>(dr);
                            s.add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            var so = s;
                            sp.orders.Add(so);
                            // sp.shop_order.Add(so);
                            sp.SaveChanges();
                            contextTransaction.Commit();
                        }
                        catch (Exception) {
                            contextTransaction.Rollback();
                        }
                    }
                    // progressBarControl2.Position += 1;
                    progressBarControl2.PerformStep();
                    //处理当前消息队列中的所有windows消息,不然进度条会不同步
                    System.Windows.Forms.Application.DoEvents();
                }
            });
        }
 

        /// <summary>
        /// 创建无参的方法
        /// </summary>
   public void Thread1(object obj)
        {
            DataTable dt = new DataTable();
            //OpenFileDialog fd = new OpenFileDialog();
            //  if (fd.ShowDialog() == DialogResult.OK)
            //{
            //  string excelFile = fd.FileName;
            //}
            for (int i = 0; i <= 1000; i++) {
                Console.WriteLine("这是无参的方法");
            }
            //  Console.WriteLine("这是无参的方法");
          //  object obj=new object();
            Entrust callback = obj as Entrust;//强转为委托
            callback("我是子线程，我执行完毕了，通知主线程");
            //子线程的循环执行完了就执行主线程的方法
            abc("jingjing");
        }
        private void ImportCallBack(ProgressBarControl progress) {

            

        }
        private  void CallBack(string str)
        {
            Console.WriteLine(str);
            // MessageBox.Show("荆凯");
          // MainForm.textEdit1.Text="555";
        }
        public void abc(string text) {
            
            if (this.textEdit1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(abc);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textEdit1.Text = text;
            }
            // textEdit1.Text = "wojingjing";
        }
        private void button4_Click(object sender, EventArgs e)
        {
            
            string sql = "select * from orders";
             var data = sp.Database.ExecuteSqlCommand(sql);
                var sss=  sp.orders.Where(s=>s.id>5).ToList();
              gridControl1.DataSource = sss;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            bb b = new bb();
            b.Show();
        }
        /**
         * 清除当前月导入的数据
         * 
         */ 
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            //获取当前月份
            DateTime now = DateTime.Now;
            string dayStart = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd HH:mm:ss");
            string dayEnd = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
            //删除sql
            // sp.orders.Where(s => (s.发货日期 >= "" ));
            // string sql  = "SELECT * from orders where 发货日期 >='"+dayStart+ "' and 发货日期<='"+dayEnd+"' ";
            //var shuju=  sp.Database.SqlQuery<orders>(sql).ToList();

            //gridControl1.DataSource = shuju;
            string sql = "DELETE FROM orders WHERE  发货日期 >='" + dayStart + "' and 发货日期<='" + dayEnd + "' ";
            DialogResult dg= DevExpress.XtraEditors.XtraMessageBox.Show("您确认删除当前月份导入的数据吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //int delint= sp.Database.ExecuteSqlCommand(sql);
            if (dg== DialogResult.OK)
            {
                int delint = sp.Database.ExecuteSqlCommand(sql);
                 button1.Enabled = true;
                 MessageBox.Show(delint.ToString()+"成功清除数据！");
            }
            else {
                MessageBox.Show("取消");
            }

        }


        /// <summary>
        /// 数据比对功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {  //读取wms表中数据
           List<edb> list = sp.edb.ToList<edb>();
            //  DataSet ds = new DataSet("jk");
            //ds.Load = list;
            //ds.
            foreach (var edbrow in list) {
              //  MessageBox.Show(edbrow.付款状态);
            }

        }
        /// <summary>
        /// 查询优易数据表中数据
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public string youyi(object o)
        {
            return "";
        }
        
        /// <summary>
        /// 临时加入的绿方达平台上传公司执照，自动添加45度水印效果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            if (op.ShowDialog() == DialogResult.OK) {

                string filename = op.FileName;
                //   UploadFile(filename,"http://www.gfonda.com/put.php");
            
                var path = Directory.GetCurrentDirectory();
                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
              string mc=  Convert.ToInt64(ts.TotalSeconds).ToString();
              string[] strr= AddWaterMark(filename,path+ "/uploads/" + mc+".jpg");
                // responseBytes = new byte[ex.Response.ContentLength];
                //获取
                WebClient myWebClient = new WebClient();
                byte[] responseArray = myWebClient.UploadFile("http://www.gfonda.com/put.php", "POST", strr[1]);
                string ass= System.Text.Encoding.UTF8.GetString(responseArray);


            }
        }

        /// <summary>

        /// 上传文件方法
        /// </summary>
        /// <param name="filePath">本地文件所在路径（包括文件）</param>
        /// <param name="serverPath">文件存储服务器路径（包括文件）</param>
        public void UploadFile(string filePath, string serverPath)
        {
            //创建WebClient实例
            WebClient webClient = new WebClient();
            webClient.Credentials = CredentialCache.DefaultCredentials;
            //要上传的文件 
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] postArray = br.ReadBytes((int)fs.Length);
            Stream postStream = webClient.OpenWrite(serverPath, "PUT");
            try
            {
                if (postStream.CanWrite)
                {
                    postStream.Write(postArray, 0, postArray.Length);
                    postStream.Close();
                    fs.Dispose();
                }
                else
                {
                    postStream.Close();
                    fs.Dispose();
                }

            }
            catch (Exception ex)
            {
                postStream.Close();
                fs.Dispose();
                throw ex;
            }
            finally
            {
                postStream.Close();
                fs.Dispose();
            }
        }

        /// <summary>
        /// 添加水印
        /// </summary>
        /// <param name="imgPath">原图片地址</param>
        /// <param name="sImgPath">水印图片地址</param>
        /// <returns>resMsg[0] 成功,失败 </returns>
        public static string[] AddWaterMark(string imgPath, string sImgPath)
        {
            string[] resMsg = new[] { "成功", sImgPath };
            using (Image image = Image.FromFile(imgPath))
            {
                try
                {
                    Bitmap bitmap = new Bitmap(image);
                    Color markcolor = Color.Red;
                    int degree = 45;
                    float alpha = 0.5f;//设置水印透明度

                    //图片的宽度与高度
                    int width = bitmap.Width, height = bitmap.Height;
                    
                    //水印文字
                    string text = "方达商城入驻专用 其他无效";

                     Graphics g = Graphics.FromImage(bitmap);
                    //Bitmap fd = new Bitmap(200,20, PixelFormat.Format32bppRgb);
                    //Graphics g = Graphics.FromImage(fd);
                    //Font crFont = new Font("微软雅黑", 12, FontStyle.Bold);
                    //SolidBrush semiTransBrush = new SolidBrush(Color.FromArgb(120, 177, 171, 171));
                    //g.DrawRectangle(new Pen(Color.FromArgb(120, Color.LightGreen),1), 1,2, 3, 4);
                    //g.DrawString(text, crFont, semiTransBrush, new PointF(0, 0));
                    //g.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.LightGreen)), 0, 0, 200, 20);

                    //   Graphics gg = Graphics.FromImage(bitmap);
                    //  gg.DrawImage(bitmap, 0, 0);

                    // gg.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                    //gg.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    //gg.TranslateTransform(width / 2, height / 2);
                    //gg.RotateTransform(-45);
                    int rowsNumber, columnsNumber;
                    if (width > height) {
                     rowsNumber = height /20;// 图片的高  除以  文字水印的宽    ——> 打印的行数(以文字水印的宽为间隔)
                     columnsNumber = width / 200;//图片的宽 除以 文字水印的宽   ——> 每行打印的列数(以文字水印的宽为间隔)
                    }else { 
                    rowsNumber = height /200;// 图片的高  除以  文字水印的宽    ——> 打印的行数(以文字水印的宽为间隔)
                     columnsNumber = width / 20;//图片的宽 除以 文字水印的宽   ——> 每行打印的列数(以文字水印的宽为间隔)
                    }
                    if (rowsNumber < 1)
                    {
                        rowsNumber = 1;
                    }
                    if (columnsNumber < 1)
                    {
                        columnsNumber = 1;
                    }

                    //  fd.MakeTransparent(Color.Transparent);
                   
                    // gg.DrawImage(fd, new PointF(0, 0));
                    //    bitmap.Save(sImgPath, System.Drawing.Imaging.ImageFormat.Jpeg);

                    // fd.Save(sImgPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    g.DrawImage(bitmap, 0, 0);

                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                    g.DrawImage(image, new Rectangle(0, 0, width, height), 0, 0, width, height, GraphicsUnit.Pixel);

                    Font crFont = new Font("微软雅黑", 12, FontStyle.Bold);
                    SizeF crSize = new SizeF();
                    crSize = g.MeasureString(text, crFont);

                    //背景位置(去掉了. 如果想用可以自己调一调 位置.)
                    //graphics.FillRectangle(new SolidBrush(Color.FromArgb(200, 255, 255, 255)), (width - crSize.Width) / 2, (height - crSize.Height) / 2, crSize.Width, crSize.Height);

                    SolidBrush semiTransBrush = new SolidBrush(Color.FromArgb(120, 137, 131, 131));

                    //将原点移动 到图片中点
                    g.TranslateTransform(0 , height );
                  //  g.TranslateTransform(0, 0);
                    //以原点为中心 转 -45度
                    g.RotateTransform(-45);

                    for (int j = 0; j < rowsNumber; j++)
                    {
                        for (int i = 0; i < columnsNumber; i++)
                        {
                            // g.drawString(waterMarkContent, i * width + j * width, -i * width + j * width);//画出水印,并设置水印位置
                           // gg.DrawImage(fd, new PointF(i * 200 + j * 200, -i * 200 + j * 200));
                            g.DrawString(text, crFont, semiTransBrush, new PointF(i * 100 + j * 200, -i * 100 + j * 200));
                        }
                    }

                   // g.DrawString(text, crFont, semiTransBrush, new PointF(0, 0));

                    //保存文件
                      bitmap.Save(sImgPath, System.Drawing.Imaging.ImageFormat.Jpeg);

                }
                catch (Exception e)
                {

                    resMsg[0] = "失败";
                    resMsg[1] = e.Message;
                }
            }

            return resMsg;
        }

        /// <summary>
        /// 管理客户用户列表页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MemmberForm memmberForm = new MemmberForm();
            memmberForm.Show();
        }

        /// <summary>
        /// 清除当天导入的数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {   //获取当前时间
            string dayStart = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            string dayEnd = DateTime.Now.ToString("yyyy-MM-dd")+" 23:59:59";
            string sql = "DELETE FROM orders WHERE  add_time >='"+ dayStart +"' and add_time<='" + dayEnd + "' ";
            int delint = sp.Database.ExecuteSqlCommand(sql);
            MessageBox.Show(delint.ToString() + "成功清除数据！");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            notifyIcon1.Visible = true;
            this.Hide();
            this.ShowInTaskbar = false;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;  //窗体状态默认大小

            this.Activate();
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;  //窗体状态默认大小

            this.Activate();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("计算数据,比较耗时，大约7分钟！");
            Hashtable ht = new Hashtable();
            //获取用户信息
            var users =sp.member.ToList();
            //获取当前时间
            DateTime now = DateTime.Now;
            string dayStart = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd HH:mm:ss");
            string dayEnd = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
         
            //无备注统计
            string sql = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
            int usertotle=users.Count;
            int count = 0;
            foreach (var user  in users) {
                count++;
                if (count == usertotle) {
                    sql += "`客服备注` not like '%" + user.username + "%' ";
                }
                else
                {
                    sql += "`客服备注` not like '%" + user.username + "%' and ";
                }
            }

           var wubeizhu = sp.Database.SqlQuery<userTotleDto>(sql).FirstOrDefault();
            ht.Add("无备注", wubeizhu);
            //单独个人业绩 一对多算法
  
            for (int i = 0; i < usertotle; i++)
            {
                string sql2 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
                var userslist =  users.Where(a=> a.username!= users[i].username.ToString()).ToList();
                sql2 += "`客服备注` like '%" + users[i].username + "%' ";
              //  int k = 0;
                foreach (var userlist in userslist) {
                //    k++;
                  //  if (k == userslist.Count) {
                         
                        sql2+= " and  `客服备注` not like '%" + userlist.username + "%' "; 
                    //}

                }
                Console.WriteLine(sql2);
                // users.Select("user_name not like '%" + users[i].username.ToString() + "%'");
                //sp.Database.SqlQuery<stir>(sql2);
                var danduyeji = sp.Database.SqlQuery<userTotleDto>(sql2).FirstOrDefault();
                ht.Add(users[i].username,danduyeji );
                sql2=null;
            }

            //两两组合算法
            for (int a=0;a<usertotle;a++) {
                
                for (int b = a + 1; b < usertotle; b++) {
                    string sql3 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
                    sql3 += " `客服备注`  like '%" + users[a].username + "%' and `客服备注`  like '%" + users[b].username + "%' ";
                    Console.WriteLine(sql3);
                    var zhuheyeji = sp.Database.SqlQuery<userTotleDto>(sql3).FirstOrDefault();
                    memoEdit1.MaskBox.AppendText(sql3 + "\r\n");
                    ht.Add(users[a].username + users[b].username, zhuheyeji);
                    sql3 = null;
                }
                      
                  }
            string sql6 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where  add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and ";
            //其他组合算法
            for (int f = 0; f < usertotle; f++)
            {
                if (f== usertotle-3) {
                    for (int g = f + 1; g < usertotle; g++)
                    {
                        for (int k = g + 1; k < usertotle; k++)
                        {
                            sql6 += "( `客服备注`  like '%" + users[f].username + "%" + users[g].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[f].username + "%" + users[k].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[k].username + "%" + users[f].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[f].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[f].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[g].username + "%" + users[f].username + "%')  ";
                        }
                    }

                } else { 
               
                for (int g = f + 1; g < usertotle; g++)
                {
                   for(int k=g+1;k < usertotle; k++) {
                         sql6 += "( `客服备注`  like '%" + users[f].username + "%" + users[g].username + "%" + users[k].username + "%' or ";
                        sql6 += " `客服备注`  like '%" + users[f].username + "%" + users[k].username + "%" + users[g].username + "%' or ";
                        sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[k].username + "%" + users[f].username + "%' or ";
                        sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[f].username + "%" + users[k].username + "%' or ";
                        sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[f].username + "%" + users[g].username + "%' or ";
                        sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[g].username + "%" + users[f].username + "%') or ";
                    }
                }
                }
             
              

            }
            var teshuyeji = sp.Database.SqlQuery<userTotleDto>(sql6).FirstOrDefault();
            ht.Add("特殊订单", teshuyeji);
            //  Console.Write(sql6);
            //写入数据库
            foreach (DictionaryEntry sj in ht)
            {
                double totle,yunfei;

                userTotleDto userDto = (userTotleDto)sj.Value;
          

                if (userDto.totle == null)
                {
                    totle = 0;
                }
                
                if (userDto.yunfei== null) {
                    yunfei = 0;
                } 

                member_achievement achievement = new member_achievement
                {
                    user_name = sj.Key.ToString(),

                    totle_price = userDto.totle,
                    add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    totle_freight = userDto.yunfei,
                    moth= DateTime.Now.ToString("yyyy-MM")
                       };
                sp.member_achievement.Add(achievement);
                sp.SaveChanges();

            }
            //自动计算店铺销售金额
            var shops = sp.shop.ToList();
            //  var shopsql = "SELECT sum(`实收金额`) as shishouprice, sum(`实收快递费`) as yunfeiprice  from orders where ";
         string   month = DateTime.Now.ToString("yyyy-MM");
           var hzlist= sp.shophuizong.Where(ss => ss.month == month).ToList();
            if (hzlist != null && hzlist.Any())

            {

                foreach(shophuizong item in hzlist)

       {

                    sp.shophuizong.Remove(item);

                }

            }

            sp.SaveChanges();
            foreach (var shop in shops) {
                //查询业绩
                //  shopsql += "店铺名称='"+shop.shop_name+"'";
                //   var teshuyej = sp.Database.SqlQuery<userTotleDto>(sql6).FirstOrDefault();
                var shopsql = "SELECT sum(`实收金额`) as shishouprice, sum(`实收快递费`) as yunfeiprice  from orders where 店铺名称='" + shop.shop_name + "' and add_time >='" + dayStart + "' and add_time<='" + dayEnd + "'  ";
                var shopyeji = sp.Database.SqlQuery<ShopDto>(shopsql).FirstOrDefault();
                shopyeji.shopname = shop.shop_name;
                //写入店铺业绩数据库
                shophuizong sphz = new shophuizong {
                    shopname = shopyeji.shopname,
                    shishouprice = shopyeji.shishouprice,
                    yunfeiprice = shopyeji.yunfeiprice,
                    month = month,
                    add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                };
                
                sp.shophuizong.Add(sphz);
                sp.SaveChanges();
            }
            }

        private void button3_Click(object sender, EventArgs e)
        {
            XtraReport1 xtraReport = new XtraReport1();
            var mlc= sp.member_achievement .ToList();
            xtraReport.DataSource = mlc;
            
              ReportPrintTool tool = new ReportPrintTool(xtraReport);
            tool.ShowPreview();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DayinForm dy = new DayinForm();
            dy.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            XtraReport1 xtraReport = new XtraReport1();
            //获取当前时间
            string mothtime = DateTime.Now.ToString("yyyy-MM");
            var mlc = sp.member_achievement.Where(s=>s.moth==mothtime).ToList();
            xtraReport.DataSource = mlc;
            //店铺业绩统计
            // var sj=sp.orders.Where
            var shopshuju= sp.shophuizong.Where(s => s.month == mothtime).ToList();
            // XRSubreport detailReport = xtraReport.FindControl("xrSubreport1", true) as XRSubreport;
            // detailReport.ReportSource.DataSource = detailReport;
            // detailReport.ParameterBindings;
            XRSubreport detailReport= xtraReport.Bands[BandKind.ReportFooter].FindControl("xrSubreport1", true) as XRSubreport;
            detailReport.ReportSource.DataSource = mlc;

            ReportPrintTool tool = new ReportPrintTool(xtraReport);
            tool.ShowPreview();

        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gerenyue gry = new gerenyue();
            gry.Show();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //获取当前月份
            DateTime now = DateTime.Now;
            string dayStart = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd HH:mm:ss");
            string dayEnd = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
            //删除sql
            // sp.orders.Where(s => (s.发货日期 >= "" ));
            // string sql  = "SELECT * from orders where 发货日期 >='"+dayStart+ "' and 发货日期<='"+dayEnd+"' ";
            //var shuju=  sp.Database.SqlQuery<orders>(sql).ToList();

            //gridControl1.DataSource = shuju;
            string sql = "DELETE FROM orders WHERE  add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' ";
            DialogResult dg = DevExpress.XtraEditors.XtraMessageBox.Show("您确认删除当前月份导入的数据吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //int delint= sp.Database.ExecuteSqlCommand(sql);
            if (dg == DialogResult.OK)
            {
                int delint = sp.Database.ExecuteSqlCommand(sql);
                button1.Enabled = true;
                MessageBox.Show(delint.ToString() + "成功清除数据！");
            }
            else
            {
                MessageBox.Show("取消");
            }
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("计算数据,比较耗时，大约7分钟！");
            Hashtable ht = new Hashtable();
            //获取用户信息
            var users = sp.member.ToList();
            //获取当前时间
            DateTime now = DateTime.Now;
            string dayStart = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd HH:mm:ss");
            string dayEnd = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");

            //无备注统计
            string sql = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
            int usertotle = users.Count;
            int count = 0;
            foreach (var user in users)
            {
                count++;
                if (count == usertotle)
                {
                    sql += "`客服备注` not like '%" + user.username + "%' ";
                }
                else
                {
                    sql += "`客服备注` not like '%" + user.username + "%' and ";
                }
            }

            var wubeizhu = sp.Database.SqlQuery<userTotleDto>(sql).FirstOrDefault();
            ht.Add("无备注", wubeizhu);
            //单独个人业绩 一对多算法

            for (int i = 0; i < usertotle; i++)
            {
                string sql2 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
                var userslist = users.Where(a => a.username != users[i].username.ToString()).ToList();
                sql2 += "`客服备注` like '%" + users[i].username + "%' ";
                //  int k = 0;
                foreach (var userlist in userslist)
                {
                    //    k++;
                    //  if (k == userslist.Count) {

                    sql2 += " and  `客服备注` not like '%" + userlist.username + "%' ";
                    //}

                }
                Console.WriteLine(sql2);
                // users.Select("user_name not like '%" + users[i].username.ToString() + "%'");
                //sp.Database.SqlQuery<stir>(sql2);
                var danduyeji = sp.Database.SqlQuery<userTotleDto>(sql2).FirstOrDefault();
                ht.Add(users[i].username, danduyeji);
                sql2 = null;
            }

            //两两组合算法
            for (int a = 0; a < usertotle; a++)
            {

                for (int b = a + 1; b < usertotle; b++)
                {
                    string sql3 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and  ";
                    sql3 += " `客服备注`  like '%" + users[a].username + "%' and `客服备注`  like '%" + users[b].username + "%' ";
                    Console.WriteLine(sql3);
                    var zhuheyeji = sp.Database.SqlQuery<userTotleDto>(sql3).FirstOrDefault();
                    memoEdit1.MaskBox.AppendText(sql3 + "\r\n");
                    ht.Add(users[a].username + users[b].username, zhuheyeji);
                    sql3 = null;
                }

            }
            string sql6 = "SELECT sum(`实收金额`) as totle, sum(`实收快递费`) as yunfei  from orders where  add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' and ";
            //其他组合算法
            for (int f = 0; f < usertotle; f++)
            {
                if (f == usertotle - 3)
                {
                    for (int g = f + 1; g < usertotle; g++)
                    {
                        for (int k = g + 1; k < usertotle; k++)
                        {
                            sql6 += "( `客服备注`  like '%" + users[f].username + "%" + users[g].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[f].username + "%" + users[k].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[k].username + "%" + users[f].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[f].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[f].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[g].username + "%" + users[f].username + "%')  ";
                        }
                    }

                }
                else
                {

                    for (int g = f + 1; g < usertotle; g++)
                    {
                        for (int k = g + 1; k < usertotle; k++)
                        {
                            sql6 += "( `客服备注`  like '%" + users[f].username + "%" + users[g].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[f].username + "%" + users[k].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[k].username + "%" + users[f].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[g].username + "%" + users[f].username + "%" + users[k].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[f].username + "%" + users[g].username + "%' or ";
                            sql6 += " `客服备注`  like '%" + users[k].username + "%" + users[g].username + "%" + users[f].username + "%') or ";
                        }
                    }
                }



            }
            var teshuyeji = sp.Database.SqlQuery<userTotleDto>(sql6).FirstOrDefault();
            ht.Add("特殊订单", teshuyeji);
            //  Console.Write(sql6);
            //写入数据库
            foreach (DictionaryEntry sj in ht)
            {
                double totle, yunfei;

                userTotleDto userDto = (userTotleDto)sj.Value;


                if (userDto.totle == null)
                {
                    totle = 0;
                }

                if (userDto.yunfei == null)
                {
                    yunfei = 0;
                }

                member_achievement achievement = new member_achievement
                {
                    user_name = sj.Key.ToString(),

                    totle_price = userDto.totle,
                    add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    totle_freight = userDto.yunfei,
                    moth = DateTime.Now.ToString("yyyy-MM")
                };
                sp.member_achievement.Add(achievement);
                sp.SaveChanges();

            }
            //自动计算店铺销售金额
            var shops = sp.shop.ToList();
            //  var shopsql = "SELECT sum(`实收金额`) as shishouprice, sum(`实收快递费`) as yunfeiprice  from orders where ";
            string month = DateTime.Now.ToString("yyyy-MM");
            var hzlist = sp.shophuizong.Where(ss => ss.month == month).ToList();
            if (hzlist != null && hzlist.Any())

            {

                foreach (shophuizong item in hzlist)

                {

                    sp.shophuizong.Remove(item);

                }

            }

            sp.SaveChanges();
            foreach (var shop in shops)
            {
                //查询业绩
                //  shopsql += "店铺名称='"+shop.shop_name+"'";
                //   var teshuyej = sp.Database.SqlQuery<userTotleDto>(sql6).FirstOrDefault();
                var shopsql = "SELECT sum(`实收金额`) as shishouprice, sum(`实收快递费`) as yunfeiprice  from orders where 店铺名称='" + shop.shop_name + "' and add_time >='" + dayStart + "' and add_time<='" + dayEnd + "'  ";
                var shopyeji = sp.Database.SqlQuery<ShopDto>(shopsql).FirstOrDefault();
                shopyeji.shopname = shop.shop_name;
                //写入店铺业绩数据库
                shophuizong sphz = new shophuizong
                {
                    shopname = shopyeji.shopname,
                    shishouprice = shopyeji.shishouprice,
                    yunfeiprice = shopyeji.yunfeiprice,
                    month = month,
                    add_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                };

                sp.shophuizong.Add(sphz);
                sp.SaveChanges();
            }
            MessageBox.Show("计算完毕！");
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //清除当前月数据
            //获取当前月份
            DateTime now = DateTime.Now;
            string dayStart = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd HH:mm:ss");
            string dayEnd = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
            //删除sql
            // sp.orders.Where(s => (s.发货日期 >= "" ));
            // string sql  = "SELECT * from orders where 发货日期 >='"+dayStart+ "' and 发货日期<='"+dayEnd+"' ";
            //var shuju=  sp.Database.SqlQuery<orders>(sql).ToList();

            //gridControl1.DataSource = shuju;
            string sql = "DELETE FROM member_achievement WHERE  add_time >='" + dayStart + "' and add_time<='" + dayEnd + "' ";
            DialogResult dg = DevExpress.XtraEditors.XtraMessageBox.Show("您确认删除当前月份导入的数据吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //int delint= sp.Database.ExecuteSqlCommand(sql);
            if (dg == DialogResult.OK)
            {
                int delint = sp.Database.ExecuteSqlCommand(sql);
                button1.Enabled = true;
                MessageBox.Show(delint.ToString() + "成功清除数据！");
            }
            else
            {
                MessageBox.Show("取消");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.Show();
        }
    }  



}