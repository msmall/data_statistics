﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;

namespace WindowsFormsCaiWuApplication
{
    public partial class gerenyue : DevExpress.XtraEditors.XtraForm
    {
        shopEntities sp = new shopEntities();
        public gerenyue()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            gridView1.Columns.Clear();
            gridControl1.Refresh();
            string riqi = dateEdit1.Text;
            var grdata=sp.member_achievement.Where(s => s.moth == riqi).ToList();
            // gridView1.DataSource = grdata;
            gridControl1.DataSource = grdata;

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {    //获取当前时间
            string mothtime = dateEdit1.Text;
            XtraReport1 xtraReport = new XtraReport1();
        
            var mlc = sp.member_achievement.Where(s => s.moth == mothtime).ToList();
            xtraReport.DataSource = mlc;
            //店铺业绩统计
            // var sj=sp.orders.Where
            var shopshuju = sp.shophuizong.Where(s => s.month == mothtime).ToList();
            // XRSubreport detailReport = xtraReport.FindControl("xrSubreport1", true) as XRSubreport;
            // detailReport.ReportSource.DataSource = detailReport;
            // detailReport.ParameterBindings;
            XRSubreport detailReport = xtraReport.Bands[BandKind.ReportFooter].FindControl("xrSubreport1", true) as XRSubreport;
            detailReport.ReportSource.DataSource = mlc;

            ReportPrintTool tool = new ReportPrintTool(xtraReport);
            tool.ShowPreview();
        }

        /// <summary>
        /// 查询店铺信息  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            gridView1.Columns.Clear();
            gridControl1.Refresh();
            string riqi = dateEdit1.Text;
           var dpdata = sp.shophuizong.Where(s => s.month == riqi).ToList();
            gridView1.RefreshData();
            gridControl1.DataSource = dpdata;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            string mothtime = dateEdit1.Text;
            XtraReport6 xtraReport = new XtraReport6();
            //获取当前时间
          
            var mlc = sp.shophuizong.Where(s => s.month == mothtime).ToList();
            xtraReport.DataSource = mlc;
            //店铺业绩统计
            // var sj=sp.orders.Where
            var shopshuju = sp.shophuizong.Where(s => s.month == mothtime).ToList();
            // XRSubreport detailReport = xtraReport.FindControl("xrSubreport1", true) as XRSubreport;
            // detailReport.ReportSource.DataSource = detailReport;
            // detailReport.ParameterBindings;
         //   XRSubreport detailReport = xtraReport.Bands[BandKind.ReportFooter].FindControl("xrSubreport1", true) as XRSubreport;
           // detailReport.ReportSource.DataSource = mlc;

            ReportPrintTool tool = new ReportPrintTool(xtraReport);
            tool.ShowPreview();
        }
    }
}