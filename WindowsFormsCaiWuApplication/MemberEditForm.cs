﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using com.fonda.caiwu.common;

namespace WindowsFormsCaiWuApplication
{

    public partial class MemberEditForm : DevExpress.XtraEditors.XtraForm
    {

        public delegate void ChangeHander(string s);
        public event ChangeHander Change;

        shopEntities sp = new shopEntities();
        string path;
        public string ids;
        public int id;
        public MemberEditForm()
        {
            InitializeComponent();
        }
        public MemberEditForm(string ids)
        {
            InitializeComponent();
            this.ids = ids;
        }
        public MemberEditForm(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "图片文件|*.bmp;*.jpg;*.jpeg;*.png";
            openFileDialog.FilterIndex = 1;
            string filePath = "";  // 存储路径
            if (openFileDialog.ShowDialog() == DialogResult.OK) {

                string filename = openFileDialog.FileName;
                FileInfo fileInfo = new FileInfo(filename);
                if (fileInfo.Length > 2048000)
                {
                    MessageBox.Show("上传的图片不能大于2M");
                }
                else
                {
                    filePath = filename;
                    int position = filePath.LastIndexOf("\\");
                    string fileName = Snowflake.Instance().GetId() + filePath.Substring(position + 1);

                    File.Copy(filePath, Application.StartupPath + "\\uploads\\" + fileName);
                    textBox1.Text =  "\\uploads\\" + fileName;
                    //pictureEdit1.BackgroundImage=Image.FromFile(Application.StartupPath + "\\uploads\\" + fileName);
                    //pbImage.ImageLocation = Application.StartupPath + "\\uploads\\" + fileName;
                    //pictureEdit1. = PictureBoxSizeMode.Zoom;
                    path = "\\uploads\\" + fileName;
                    pictureEdit1.Image= Image.FromFile(Application.StartupPath +path);
                    //  pictureBox1.BackgroundImage = Image.FromFile(strPicPath);  // picReceiptLogo是存储图片的路径
                }
             

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.ids);
            var ewm_member = sp.member.Find(id);
            sp.member.Attach(ewm_member);
            //ewm_member = new member{ username = textEdit1.Text,
            //    sex = checkedComboBoxEdit1.Properties.GetCheckedItems().ToString(),
            //    entry_data = textEdit2.Text,
            //    portrait = textBox1.Text };
            // sp.Entry(ewm_common).Property(a => a.image).IsModified = true;
            // ewm_common.image = path;
            ewm_member.username = textEdit1.Text;
            ewm_member.sex = checkedComboBoxEdit1.Properties.GetCheckedItems().ToString();
            ewm_member.entry_data = textEdit2.Text;
            ewm_member.portrait = textBox1.Text;
          //  ewm_member.im
            //保存回数据库
            // sp.SaveChanges();
            //var member = new member {
            //    id = id,
            //    username = textEdit1.Text,
            //    sex = checkedComboBoxEdit1.Properties.GetCheckedItems().ToString(),
            //    entry_data = textEdit2.Text,
            //    portrait = textBox1.Text
            //};
            //    sp.Entry(member).State = EntityState.Modified;
            /// sp.member.AddOrUpdate(member);
            //保存回数据库
            sp.SaveChanges();
            MessageBox.Show("修改成功！");
            //sp.member.AddOrUpdate(member);
            //   UserInfo userinfo = new UserInfo() { ID = 1, Name = "苏轼" };
            // sp.member.Attach(member);
            // db.Entry(model).State = EntityState.Unchanged;
            //db.Entry(model).Property(m => m.字段名).IsModified = true;
            //db.SaveChanges();
            //sp.Entry(member).State = EntityState.Unchanged;
            //sp.Entry(member).Property(a => a.id).IsModified = true;
            //// sp.member.Add(member);
            //sp.SaveChanges();
        }

        private void MemberEditForm_Load(object sender, EventArgs e)
        {
            int id = int.Parse(this.ids);
            //var list= sp.member.Where(s => s.id.Equals(this.ids)).ToList();
            member m = sp.member.Where(s => s.id == id).First();
            textEdit1.Text = m.username;
            textEdit2.Text = m.entry_data;
            checkedComboBoxEdit1.Text = m.sex;
            if (m.portrait != null) { 

            pictureEdit1.Image = Image.FromFile(Application.StartupPath + m.portrait);
            textBox1.Text = m.portrait;
            }

        }

        private void MemberEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //  MemmberForm

            if (Change != null)
            {
                Change("666");
            }
          
        }
    }
}