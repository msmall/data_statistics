﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;
using System.IO;

namespace WindowsFormsCaiWuApplication
{
    public partial class XtraForm1 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm1()
        {
            InitializeComponent();
        }
        XtraReport3 rpt = new XtraReport3();
        //将报表与控件关联
      
        private void XtraForm1_Load(object sender, EventArgs e)
        {
            documentViewer1.DocumentSource = rpt;
            //这一句必须要，参数为true则是在后台加载，报表数据量大的时候建议采用这种方式
            //没有参数的加载方式就是等所有数据加载好了再显示，不推荐
            XRDesignRibbonForm designForm = new XRDesignRibbonForm();
            //加载要设计的报表
            designForm.OpenReport(rpt);
            //隐藏部分按钮
            designForm.DesignRibbonController.XRDesignPanel.SetCommandVisibility(new ReportCommand[]{
                ReportCommand.NewReport,
                ReportCommand.SaveFileAs,
                ReportCommand.NewReportWizard,
                ReportCommand.OpenFile
            }, CommandVisibility.None);
            // 设置各面板的停靠位置以及是否显示 
            designForm.DesignDockManager[DesignDockPanelType.PropertyGrid].Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            designForm.DesignDockManager[DesignDockPanelType.PropertyGrid].DockAsTab(designForm.DesignDockManager[DesignDockPanelType.FieldList], 0);
            designForm.DesignDockManager[DesignDockPanelType.ErrorList].DockTo(DevExpress.XtraBars.Docking.DockingStyle.Left);
            designForm.DesignDockManager[DesignDockPanelType.GroupAndSort].DockAsTab(designForm.DesignDockManager[DesignDockPanelType.ErrorList], 0);
         //   designForm.DesignDockManager[DesignDockPanelType.ToolBox].Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            //添加设计器关闭时的事件
            designForm.FormClosing += new FormClosingEventHandler(designForm_FormClosing);
            //报表状态发生改变时触发的事件
            designForm.DesignRibbonController.XRDesignPanel.ReportStateChanged += new ReportStateEventHandler(XRDesignPanel_ReportStateChanged);
            // 打开设计器
            designForm.ShowDialog();
         
        }
        void XRDesignPanel_ReportStateChanged(object sender, ReportStateEventArgs e)
        {
            if (e.ReportState == ReportState.Changed)
            {
                //状态发生改变，立刻设为保存状态
                ((XRDesignPanel)sender).ReportState = ReportState.Saved;
            }
        }
        private void designForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("是否保存更改？", "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (dlgResult == DialogResult.Yes)
            {
                //这里来执行保存的操作，建议保存为流，这样可以考虑存数据库
                MemoryStream ms = new MemoryStream();
                rpt.SaveLayout(ms);
                //do the save things...
            }
            else if (dlgResult == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

    }
}