﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace WindowsFormsCaiWuApplication
{
    public partial class MemberAddForm : DevExpress.XtraEditors.XtraForm
    {
        shopEntities sp = new shopEntities();
        string path;
        public MemberAddForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "图片文件|*.bmp;*.jpg;*.jpeg;*.png";
            openFileDialog.FilterIndex = 1;
            string filePath = "";  // 存储路径
            if (openFileDialog.ShowDialog() == DialogResult.OK) {

                string filename = openFileDialog.FileName;
                FileInfo fileInfo = new FileInfo(filename);
                if (fileInfo.Length > 2048000)
                {
                    MessageBox.Show("上传的图片不能大于2M");
                }
                else
                {
                    filePath = filename;
                    int position = filePath.LastIndexOf("\\");
                    string fileName = filePath.Substring(position + 1);

                    File.Copy(filePath, Application.StartupPath + "\\uploads\\" + fileName);
                    textBox1.Text = Application.StartupPath + "\\uploads\\" + fileName;
                    //pictureEdit1.BackgroundImage=Image.FromFile(Application.StartupPath + "\\uploads\\" + fileName);
                    //pbImage.ImageLocation = Application.StartupPath + "\\uploads\\" + fileName;
                    //pictureEdit1. = PictureBoxSizeMode.Zoom;
                    path = "\\uploads\\" + fileName;
                    pictureEdit1.Image= Image.FromFile(Application.StartupPath + "\\uploads\\" + fileName);
                    //  pictureBox1.BackgroundImage = Image.FromFile(strPicPath);  // picReceiptLogo是存储图片的路径
                }
             

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var member = new member {
                username = textEdit1.Text,
                sex = checkedComboBoxEdit1.Properties.GetCheckedItems().ToString(),
                entry_data = textEdit2.Text,
                portrait = path
            };
            sp.member.Add(member);
            sp.SaveChanges();
        }
    }
}