﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrinting;
using System.IO;
using System.Data.Entity.Migrations;
using com.fonda.caiwu.common;

namespace WindowsFormsCaiWuApplication
{
    public partial class DayinForm : DevExpress.XtraEditors.XtraForm
    {
        shopEntities sp = new shopEntities();
        string path;
        public DayinForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cpname = textBox1.Text;
            string cpguige = textBox2.Text;
            string addtime= DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            tiaoma tm = new tiaoma {
                product_name = cpname,
                product_guige=cpguige,
                tiaoma1=textBox3.Text,
                add_time=addtime
            };
            sp.tiaoma.Add(tm);
            sp.SaveChanges();
            MessageBox.Show("添加成功！");
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = DateTime.Now.ToString("yyyyMMddHHmmss");
            var ttm = sp.tiaoma.ToList();
            gridControl1.DataSource = ttm;

        }

        private void DayinForm_Load(object sender, EventArgs e)
        {
            string tiaoma=DateTime.Now.ToString("yyyyMMddHHmmss");
            textBox3.Text = tiaoma;
            var ttm = sp.tiaoma.ToList();
            gridControl1.DataSource = ttm;
            var ewmcomm=sp.ewm_common.FirstOrDefault();
            textBox5.Text = ewmcomm.address;
            if (ewmcomm.image != null) { 
            pictureEdit1.Image= Bitmap.FromFile(System.Windows.Forms.Application.StartupPath + ewmcomm.image);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            XtraReport3 xtraReport = new XtraReport3();
            //获取当前时间
            string dayStart = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            string dayEnd = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59";

            string sql = "select id,product_name,product_guige,tiaoma as tiaoma1,add_time  from tiaoma where  add_time >='" + dayStart + "' and add_time<='" + dayEnd + "'  ";
            //  var mlc = sp.Database.SqlQuery<object>(sql).ToList();
            // var mlc = sp.tiaoma.Where(s => double.Parse(s.add_time)>=dayStart);
            //var mlc = sp.tiaoma.ToList();
            var mlc = sp.tiaoma.SqlQuery(sql).AsQueryable();
            xtraReport.DataSource = mlc;
            //   XRSubreport xRSubreport = (XRSubreport) xtraReport.FindControl("xrSubreport1", true);
            var sj = sp.ewm_common.ToList();
            // XRSubreport detailReport = xtraReport.FindControl("xrSubreport1", true) as XRSubreport;

            //detailReport.ReportSource.DataSource = sj;
           XRPictureBox xRPicture = xtraReport.FindControl("xrPictureBox1", true) as XRPictureBox;
           XRLabel label= xtraReport.FindControl("xrLabel6", true) as XRLabel;
            var ewmcom=sp.ewm_common.First();
            xRPicture.Image = Bitmap.FromFile(System.Windows.Forms.Application.StartupPath + ewmcom.image);
            label.Text = ewmcom.address;
            ReportPrintTool tool = new ReportPrintTool(xtraReport);



            //操作要显示什么按钮
            tool.PrintingSystem.SetCommandVisibility(new PrintingSystemCommand[]{
                    PrintingSystemCommand.Open,
                    PrintingSystemCommand.Save,
                    PrintingSystemCommand.ClosePreview,
                    PrintingSystemCommand.Customize,
                    PrintingSystemCommand.SendCsv,
                    PrintingSystemCommand.SendFile,
                    PrintingSystemCommand.SendGraphic,
                    PrintingSystemCommand.SendMht,
                    PrintingSystemCommand.SendPdf,
                    PrintingSystemCommand.SendRtf,
                    PrintingSystemCommand.SendTxt,
                    PrintingSystemCommand.SendXls
                }, CommandVisibility.None);
            tool.ShowPreview();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XtraForm1 x = new XtraForm1();
            x.Show();


        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "图片文件|*.bmp;*.jpg;*.jpeg;*.png";
            openFileDialog.FilterIndex = 1;
            string filePath = "";  // 存储路径
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

                string filename = openFileDialog.FileName;
                FileInfo fileInfo = new FileInfo(filename);
                if (fileInfo.Length > 2048000)
                {
                    MessageBox.Show("上传的图片不能大于2M");
                }
                else
                {
                    filePath = filename;
                    int position = filePath.LastIndexOf("\\");
                    string fileName = Snowflake.Instance().GetId() + filePath.Substring(position + 1);
                  //  fileName += Snowflake.Instance().GetId()+fileName;

                    File.Copy(filePath, Application.StartupPath + "\\uploads\\" + fileName);
                    textBox1.Text = Application.StartupPath + "\\uploads\\" + fileName;
                    //pictureEdit1.BackgroundImage=Image.FromFile(Application.StartupPath + "\\uploads\\" + fileName);
                    //pbImage.ImageLocation = Application.StartupPath + "\\uploads\\" + fileName;
                    //pictureEdit1. = PictureBoxSizeMode.Zoom;
                    path = "\\uploads\\" + fileName;
                    pictureEdit1.Image = Image.FromFile(Application.StartupPath + "\\uploads\\" + fileName);
                    //  pictureBox1.BackgroundImage = Image.FromFile(strPicPath);  // picReceiptLogo是存储图片的路径
                    // ewm_common ewm_common = new ewm_common() {
                    //   id=1,
                    // image=path
                    // };
                    //   sp.ewm_common.AddOrUpdate(ewm_common);
                    var ewm_common = sp.ewm_common.Find(1);
                    sp.ewm_common.Attach(ewm_common);
                    // sp.Entry(ewm_common).Property(a => a.image).IsModified = true;
                    ewm_common.image = path;
                    //保存回数据库
                    sp.SaveChanges();
                    MessageBox.Show("上传成功！");
                }


            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //   ewm_common ewm_common = new ewm_common
            // {
            //   id = 1,
            //  address = textBox5.Text
            //};
            // sp.ewm_common.AddOrUpdate(ewm_common);
            var ewm_common = sp.ewm_common.Find(1);
            sp.ewm_common.Attach(ewm_common);
            //保存回数据库
            ewm_common.address = textBox5.Text;
            sp.SaveChanges();
            MessageBox.Show("地址保存成功！");
        }
    }
}