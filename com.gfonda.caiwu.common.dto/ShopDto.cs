﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.gfonda.caiwu.common.dto
{
    public class ShopDto
    {
        public string shopname { get; set; }
        public double? shishouprice { get; set; }
        public double? yunfeiprice { get; set; } 

    }
}
